<?php
/**
 * Created by PhpStorm.
 * User: smart comp
 * Date: 6/1/2019
 * Time: 2:56 PM
 */
namespace App\Helper;

use App\Options;
use Illuminate\Support\Facades\DB;

class ED{
    public static function getOptionsByName($col_name){
        $value = Options::where('option_name', $col_name)->first()->option_value;
        return $value;
    }

    public static function successAlert($message, $type='success'){

        ?>
        <div id="success-alert" class="alert alert-<?php echo $type ?>" style="background-color:#28a745">

            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

            <h3 style="color:#fff"><strong><i class="fa fa-check" aria-hidden="true"></i></strong> Alert!</h3>

            <p style="color:#fff"><?php echo $message; ?></p>

        </div>
        <?php

    }

    public static function upload_files($request, $uplaod){

        if($request->hasfile($uplaod)){
            echo 'yes';
            $file = $request->file($uplaod);
            $extension = $file->getClientOriginalExtension();
            $filename = time() . $uplaod . '.' . $extension;
            $file->move('images/', $filename);
            return $filename;
        }

        return $request->upload;

    }

    public static  function convertYoutube($url, $width, $height) {

        echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
            "<iframe width=\"$width\" height=\"$height\" src=\"//www.youtube.com/embed/$1\"         frameborder=\"0\" allowfullscreen></iframe>", $url);
    }

    public static function contact_us_form(){

        ?>



        <?php

    }

}