<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unique_data extends Model
{
	protected $updated_at = false;
	protected $created_at = false;
    public $timestamps = false;
	
    protected $fillable = [
        "AdId", "AdName", "AdUrl", "AdInfo", "AdPostCode", "AdCity",
        "AdDate", "AdTime", "FromURL", "AdListCategory",'AdCategory', "AdNumber",
        "AdOrt", "AdStundenlohn", "AdArt", "AdText","AdPersonName","AdPersonTel","ExportTimeStamp", "BatchId", "scraped_id"
    ];

    protected $table ="unique_data";

//    public function category()
//    {
//        return $this->belongsTo('App\Category');
//    }
}
