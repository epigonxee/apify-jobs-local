<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Swap_list extends Model
{
    public $timestamps = false;
    protected $fillable = [
        "old_value", "new_value"
    ];


    protected $table ="swapper_list";
}