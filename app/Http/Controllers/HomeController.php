<?php

namespace App\Http\Controllers;

use App\Scraped_data;
use Composer\Command\SearchCommand;
use Illuminate\Http\Request;
use App\Swap_list;
use App\Unique_data;

use Grids;
use HTML;
use Illuminate\Support\Facades\Config;
use Nayjest\Grids\Components\Base\RenderableRegistry;
use Nayjest\Grids\Components\ColumnHeadersRow;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\Filters\DateRangePicker;
use Nayjest\Grids\Components\FiltersRow;
use Nayjest\Grids\Components\HtmlTag;
use Nayjest\Grids\Components\Laravel5\Pager;
use Nayjest\Grids\Components\OneCellRow;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\RenderFunc;
use Nayjest\Grids\Components\ShowingRecords;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Nayjest\Grids\Components\TotalsRow;
use Nayjest\Grids\DbalDataProvider;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\FilterConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('layouts.app');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function show_swap_lists()
    {
        $s_lists = Swap_list::all();


        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider(Swap_list::query())
                )
                ->setName('swapper')
                ->setPageSize(5)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('ID')
                        ->setSortable(true)
                        ->setSorting(Grid::SORT_ASC)
                    ,
                    (new FieldConfig)
                        ->setName('old_value')
                        ->setLabel('Old Value')
                        ->setCallback(function ($val) {
                            $GLOBALS['old_val'] = $val;
                            return $val;
                        })
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('new_value')
                        ->setLabel('New Value')
                        ->setCallback(function ($val) {
                            $GLOBALS['new_val'] = $val;
                            return $val;
                        })
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        ),


                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link('', "EDIT", 'data-id="' . $val . '" data-new="' . $GLOBALS['new_val'] . '" data-old="' . $GLOBALS['old_val'] . '" class="update-filter" data-toggle="modal" data-target="#modal-update"')
                                . '</small>'
                                . '<small>'
                                . $del
                                . HTML::link(route('delete_filter', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            (new ColumnHeadersRow),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
//                                    new ColumnsHider,
                                    (new CsvExport)
                                        ->setFileName('my_report' . date('Y-m-d'))
                                    ,
//                                    new ExcelExport(),
                                    (new HtmlTag)
                                        ->setContent('<span class="glyphicon glyphicon-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );


        $grid = $grid->render();
        // var_dump($grid); exit;
        return view('swap_list.index', compact('s_lists', 'grid'));
    }

    public function add_filter(Request $request)
    {

        // var_dump($request); exit;
        Swap_list::create($request->all());
        return redirect()->back()->with('message', 'Filter! Created');
    }


    public function update_filter(Request $request)
    {
        Swap_list::where('id', $request->id)->update(['old_value' => $request->old_value, 'new_value' => $request->new_value]);
        return redirect()->back()->with('message', 'Filter! Updated');
    }

    public function delete_filter(Swap_list $id)
    {
        $id->delete();
        return redirect()->back()->with('message', 'Filter! Deleted');
    }


    //Scraped Data

    public function index_scraped_data()
    {

        $query = Scraped_data::query();
        //var_dump(Scraped_data::all()); exit;

        //var_dump($query); exit;
        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('swapper')
                ->setPageSize(30)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('AdId')
                        ->setLabel('AdId')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('ExportTimeStamp')
                        ->setLabel('ExportTimeStamp')
                        ->setSortable(true)

                    ,
                    (new FieldConfig)
                        ->setName('AdUrl')
                        ->setLabel('AdUrl')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdName')
                        ->setLabel('AdName')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdInfo')
                        ->setLabel('AdInfo')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPostCode')
                        ->setLabel('AdPostCode')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )

                    ,
                    (new FieldConfig)
                        ->setName('AdCity')
                        ->setLabel('AdCity')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdDate')
                        ->setLabel('AdDate')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdTime')
                        ->setLabel('AdTime')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('FromURL')
                        ->setLabel('FromURL')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdListCategory')
                        ->setLabel('AdListCategory')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdCategory')
                        ->setLabel('AdCategory')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdNumber')
                        ->setLabel('AdNumber')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdOrt')
                        ->setLabel('AdOrt')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdStundenlohn')
                        ->setLabel('AdStundenlohn')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdArt')
                        ->setLabel('AdArt')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdPersonName')
                        ->setLabel('AdPersonName')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdPersonTel')
                        ->setLabel('AdPersonTel')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,


                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link(route('u_form_scraped', ['id' => $val]), "EDIT")
                                . '</small><br>'
                                . '<small style="margin-right:20px;">'
                                . $del
                                . HTML::link(route('delete_scraped_data', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                            //(new HtmlTag)->setAttributes(['class' => 'float-right'])->addComponent(new Pager),
                            (new HtmlTag)
                                ->setAttributes(['class' => 'float-left pb-2'])
                                ->addComponent(new ShowingRecords)
                            ,
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                                ->addComponents([
                                    (new RenderFunc(function () {
                                        return "<style>
                                                .daterangepicker td.available.active,
                                                .daterangepicker li.active,
                                                .daterangepicker li:hover {
                                                    color:black !important;
                                                    font-weight: bold;
                                                }
                                           </style>";
                                    }))
                                        ->setRenderSection('filters_row_column_ExportTimeStamp'),
                                    (new DateRangePicker)
                                        ->setName('ExportTimeStamp')
                                        ->setRenderSection('filters_row_column_ExportTimeStamp')
                                        ->setDefaultValue([date("Y-m-d", strtotime("-3 months")), date('Y-m-d')])
                                ]),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
                                   // new ColumnsHider(),
                                    (new CsvExport)->setFileName('my_report' . date('Y-m-d')),
                                    new ExcelExport(),
                                    (new HtmlTag)
                                        ->setContent('<span class="fas fa-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );

        $grid = $grid->render();
        return view('scraped_data.index', compact('grid'));
    }

    public function scraped_form($id = '')
    {
        if ($id) {
            $edit = Scraped_data::where('id', $id)->first();
            return view('scraped_data.form', compact('edit'));
        }
        return view('scraped_data.form');
    }

    public function add_scraped_data(Request $request)
    {
        Scraped_data::create($request->all());
        return redirect()->route('scraped_data')->with('message', 'Data Added!');
    }

    public function update_scraped_data(Scraped_data $id, Request $request)
    {
        $id->update($request->all());
        return redirect()->route('scraped_data')->with('message', 'Data Updated!');
    }

    public function delete_scraped_data(Scraped_data $id)
    {
        $id->delete();
        return redirect()->back()->with('message', 'Data Deleted!!');
    }

    //Unique Data

    public function index_unique_data()
    {

        $query = Unique_data::query();
        // var_dump($query); exit;
        //var_dump(DB::select('select AdPrice from scraped_data')); exit;
        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('swapper')
                ->setPageSize(30)
                ->setColumns([
                    (new FieldConfig)
                        ->setName('scraped_id')
                        ->setLabel('ScrapedId')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdId')
                        ->setLabel('AdId')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('ExportTimeStamp')
                        ->setLabel('ExportTimeStamp')
                        ->setSortable(true)

                    ,
                    (new FieldConfig)
                        ->setName('AdUrl')
                        ->setLabel('AdUrl')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdName')
                        ->setLabel('AdName')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdInfo')
                        ->setLabel('AdInfo')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdPostCode')
                        ->setLabel('AdPostCode')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )

                    ,
                    (new FieldConfig)
                        ->setName('AdCity')
                        ->setLabel('AdCity')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdDate')
                        ->setLabel('AdDate')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdTime')
                        ->setLabel('AdTime')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('FromURL')
                        ->setLabel('FromURL')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdListCategory')
                        ->setLabel('AdListCategory')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdCategory')
                        ->setLabel('AdCategory')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )


                    ,
                    (new FieldConfig)
                        ->setName('AdNumber')
                        ->setLabel('AdNumber')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,
                    (new FieldConfig)
                        ->setName('AdOrt')
                        ->setLabel('AdOrt')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdStundenlohn')
                        ->setLabel('AdStundenlohn')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdArt')
                        ->setLabel('AdArt')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdPersonName')
                        ->setLabel('AdPersonName')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('AdPersonTel')
                        ->setLabel('AdPersonTel')
                        ->setSortable(true)
                        ->addFilter(
                            (new FilterConfig)
                                ->setOperator(FilterConfig::OPERATOR_LIKE)
                        )
                    ,

                    (new FieldConfig)
                        ->setName('id')
                        ->setLabel('Action')
                        ->setCallback(function ($val) {
                            $edit = '<span class="fas fa-edit"></span>&nbsp;';
                            $del = '<span class="fas fa-trash"></span>&nbsp;';
                            return
                                '<small style="margin-right:10px;">'
                                . $edit
                                . HTML::link(route('u_form_unique', ['id' => $val]), "EDIT")
                                . '</small><br>'
                                . '<small style="margin-right:20px;">'
                                . $del
                                . HTML::link(route('delete_unique_data', ['id' => $val]), "DELETE")
                                . '</small>';

                        })
                    ,

                ])
                ->setComponents([
                    (new THead)
                        ->setComponents([
                        //    (new HtmlTag)->setAttributes(['class' => 'float-right'])->addComponent(new Pager),
                            (new HtmlTag)
                                ->setAttributes(['class' => 'float-left pb-2'])
                                ->addComponent(new ShowingRecords)
                            ,
                            (new ColumnHeadersRow),
                            (new FiltersRow)
                                ->addComponents([
                                    (new RenderFunc(function () {
                                        return "<style>
                                                .daterangepicker td.available.active,
                                                .daterangepicker li.active,
                                                .daterangepicker li:hover {
                                                    color:black !important;
                                                    font-weight: bold;
                                                }
                                           </style>";
                                    }))
                                        ->setRenderSection('filters_row_column_ExportTimeStamp'),
                                    (new DateRangePicker)
                                        ->setName('ExportTimeStamp')
                                        ->setRenderSection('filters_row_column_ExportTimeStamp')
                                        ->setDefaultValue([date("Y-m-d", strtotime("-3 months")), date('Y-m-d')])
                                ]),
                            (new OneCellRow)
                                ->setRenderSection(RenderableRegistry::SECTION_END)
                                ->setComponents([
                                    new RecordsPerPage,
                                //    new ColumnsHider(),
                                    (new CsvExport)->setFileName('my_report' . date('Y-m-d')),
                                    new ExcelExport(),
                                    (new HtmlTag)
                                        ->setContent('<span class="fas fa-refresh"></span> Filter')
                                        ->setTagName('button')
                                        ->setRenderSection(RenderableRegistry::SECTION_END)
                                        ->setAttributes([
                                            'class' => 'btn btn-success btn-sm'
                                        ])
                                ])
                        ])
                    ,
                    (new TFoot)
                        ->setComponents([

                            (new OneCellRow)
                                ->setComponents([
                                    new Pager,
                                    (new HtmlTag)
                                        ->setAttributes(['class' => 'pull-right'])
                                        ->addComponent(new ShowingRecords)
                                    ,
                                ])
                        ])
                    ,
                ])
        );

        $grid = $grid->render();
        return view('unique_data.index', compact('grid'));
    }

    public function unique_form($id = '')
    {
        if ($id) {
            $edit = Unique_data::where('id', $id)->first();
            return view('unique_data.form', compact('edit'));
        }
        return view('unique_data.form');
    }

    public function add_unique_data(Request $request)
    {
        Unique_data::create($request->all());
        return redirect()->route('unique_data')->with('message', 'Data Added!');
    }

    public function update_unique_data(Unique_data $id, Request $request)
    {
        $id->update($request->all());
        return redirect()->route('unique_data')->with('message', 'Data Updated!');
    }

    public function delete_unique_data(Unique_data $id)
    {
        $id->delete();
        return redirect()->back()->with('message', 'Data Deleted!!');
    }

    //SQL STATEMENTS



    public function sql_index(){
        return view('sql_table.index');
    }

    public function sql_run(Request $request){

        $sql_statement = htmlspecialchars($request->sql_statement, ENT_QUOTES);
      //  var_dump($sql_statement); exit;
        $results = '';
        $error = '';
        try {
            $results = DB::select(DB::raw($sql_statement));
        } catch(\Illuminate\Database\QueryException $ex){
            $error = $ex->getMessage();
            // Note any method of class PDOException can be called on $ex.
        }

        return view('sql_table.index', compact('results', 'sql_statement', 'error'));
    }


}
