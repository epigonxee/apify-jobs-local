<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Provider\DateTime;
use App\Scraped_data;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Visitor;
use Carbon\Carbon;
use App\Swap_list;

class Scrapy extends Controller
{


    protected $filters;

    public function __construct()
    {
        $this->filters = Swap_list::all();
    }

    public function sendData(Request $request)
    {

        $json = file_get_contents('php://input');
        $action = json_decode($json, true);
		
        $taskId = $action['resource']['actorTaskId'];
      //  $runId = $action['resource']['actorRunId'];


        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch1, CURLOPT_URL, 'https://api.apify.com/v2/actor-tasks/' . $taskId . '/runs/last/dataset/items?token=RxxJnwxk3GkRTcQgsP3DKGHk8&ui=1');
        curl_setopt($ch1, CURLOPT_HEADER, 0);
        curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);

        $result_json1 = curl_exec($ch1);
		
        curl_close($ch1);

        $scrap_data = json_decode($result_json1);

        if (!empty($scrap_data)) {

            foreach ($scrap_data as $data) {

                if(!isset($data->AdID)) continue;

                $now = new \DateTime('now', new \DateTimeZone( 'Europe/Berlin' ));
                $now = $now->format('Y-m-d H:i:s.u');
                //if (!Scraped_data::where('AdUrl', $data->f01_AdUrl)->exists()) {

                    $datatobeSaved = array(

                        'AdId' => $data->AdID,
                        'AdName' => $this->purifyData($data->f01_AdName),
                        'AdUrl' => $this->purifyData($data->f00_AdUrl),
                        'AdInfo' => $this->purifyData($data->f02_AdInfo),
                        'AdPostCode' => $data->f03_AdPostCode,
                        'AdCity' => $data->f04_AdCity,
                        'AdDate' => $data->f05_AdDate,
                        'AdTime' => $data->f06_AdTime,
                        'FromURL' => $data->f07_FromURL,
                        'AdListCategory' => $data->f08_AdListCategory,
                        'AdCategory' => $data->f09_AdCategory,
                        'AdNumber' => $data->f10_AdNumber,
                        'AdOrt' => $data->f11_AdOrt,
                        'AdStundenlohn' => $this->purifyData($data->f12_AdStundenlohn),
                        'AdArt' => $data->f13_AdArt,
                        'AdText' => $this->purifyData($data->f14_AdText),
                        'AdPersonName' => $data->f15_AdPersonName,
                        'AdPersonTel' => $data->f16_AdPersonTel,
                        'ExportTimeStamp' => $now,
                        'BatchId' => $taskId
                    );

                    Scraped_data::create($datatobeSaved);

                //}

			}
            http_response_code();
			//exit;

        } else {
            echo 'NO DATA';
			exit;
        }
		exit;
    }

    public function purifyData($str){

        $cleanStr = preg_replace('/\s\s+/', ' ', preg_replace('/[^(\x20-\x7F)]*/','', $str));
        $cleanStr = iconv(mb_detect_encoding($cleanStr, mb_detect_order(), true), "UTF-8", $cleanStr);

        foreach($this->filters as $f){

            if($f->old_value == $cleanStr){
                $cleanStr = $f->new_value;
            }

        }

        return $cleanStr;
    }

    public function debug(){

        $taskId = 'B2J3y9EY7ofERZtp9'; // type 2;
        //$taskId = '6pMQSJ59z2Zggs7rP'; // type 3;

        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch1, CURLOPT_URL, 'https://api.apify.com/v2/actor-tasks/' . $taskId . '/runs/last/dataset/items?token=RxxJnwxk3GkRTcQgsP3DKGHk8&ui=1');
        curl_setopt($ch1, CURLOPT_HEADER, 0);
        curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);

        $result_json1 = curl_exec($ch1);

        curl_close($ch1);

        $scrap_data = json_decode($result_json1);

        if (!empty($scrap_data)) {

            foreach ($scrap_data as $data) {
               // if(!isset($data->AdID)) continue;
                var_dump($data);

            }
        }

        exit;
    }

}