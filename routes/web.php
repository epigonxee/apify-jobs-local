<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('/send' ,'Scrapy@sendData')->name('s_filter');
Route::any('/debug' ,'Scrapy@debug')->name('debug_frontend');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/login', 'HomeController@login')->name('login');
Route::any('/register', 'HomeController@register')->name('register');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::post('/login_check', 'Auth\LoginController@login_check')->name('login_check');
Route::post('create-user', 'Auth\RegisterController@store')->name('create_user');


Route::prefix('/dashboard')->middleware('auth')->group(function(){

    Route::any('/', 'HomeController@index')->name('dashboard');
    Route::any('/swap-lists', 'HomeController@show_swap_lists')->name('show_swap_lists');
    Route::any('/swap-lists/edit', 'HomeController@update_filter')->name('update_filter');
    Route::any('/swap-lists/delete/{id}', 'HomeController@delete_filter')->name('delete_filter');
    Route::any('/swap-lists/add/', 'HomeController@add_filter')->name('add_filter');

    Route::any('/scraped-data', 'HomeController@index_scraped_data')->name('scraped_data');
    Route::any('/scraped-data/add', 'HomeController@scraped_form')->name('a_form_scraped');
    Route::any('/scraped-data/edit/{id}', 'HomeController@scraped_form')->name('u_form_scraped');
    Route::any('/scraped-data/save', 'HomeController@add_scraped_data')->name('add_scraped_data');
    Route::any('/scraped-data/update/{id}', 'HomeController@update_scraped_data')->name('update_scraped_data');
    Route::any('/scraped-data/delete/{id}', 'HomeController@delete_scraped_data')->name('delete_scraped_data');

    Route::any('/unique-data', 'HomeController@index_unique_data')->name('unique_data');
    Route::any('/unique-data/add', 'HomeController@unique_form')->name('a_form_unique');
    Route::any('/unique-data/edit/{id}', 'HomeController@unique_form')->name('u_form_unique');
    Route::any('/unique-data/save', 'HomeController@add_unique_data')->name('add_unique_data');
    Route::any('/unique-data/update/{id}', 'HomeController@update_unique_data')->name('update_unique_data');
    Route::any('/unique-data/delete/{id}', 'HomeController@delete_unique_data')->name('delete_unique_data');

    Route::any('/sql-statements', 'HomeController@sql_index')->name('sql_index');
    Route::any('/sql-statements/result', 'HomeController@sql_run')->name('sql_run');




});


