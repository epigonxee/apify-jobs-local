-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: apify-crawler.c7y6sginlqcw.us-east-1.rds.amazonaws.com:3306
-- Generation Time: Oct 06, 2019 at 11:08 PM
-- Server version: 5.7.22-log
-- PHP Version: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apify-scrapping-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `scraped_data`
--

CREATE TABLE `scraped_data` (
  `id` int(100) NOT NULL,
  `AdId` int(100) DEFAULT NULL,
  `AdUrl` varchar(500) DEFAULT NULL,
  `AdInfo` text,
  `AdPrice` varchar(500) DEFAULT NULL,
  `AdPostcode` varchar(500) DEFAULT NULL,
  `AdDistrict` varchar(500) DEFAULT NULL,
  `AdRooms` varchar(500) DEFAULT NULL,
  `AdSquaremeters` varchar(500) DEFAULT NULL,
  `AdDate` varchar(500) DEFAULT NULL,
  `AdTime` varchar(500) DEFAULT NULL,
  `AdCity` varchar(500) DEFAULT NULL,
  `AdPricePerSquaremeters` varchar(500) DEFAULT NULL,
  `AdFromUrl` varchar(500) DEFAULT NULL,
  `ExportDate` varchar(500) DEFAULT NULL,
  `ExportTime` varchar(500) DEFAULT NULL,
  `ExportTimeStamp` varchar(500) DEFAULT NULL,
  `BatchId` varchar(500) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scraped_data`
--

INSERT INTO `scraped_data` (`id`, `AdId`, `AdUrl`, `AdInfo`, `AdPrice`, `AdPostcode`, `AdDistrict`, `AdRooms`, `AdSquaremeters`, `AdDate`, `AdTime`, `AdCity`, `AdPricePerSquaremeters`, `AdFromUrl`, `ExportDate`, `ExportTime`, `ExportTimeStamp`, `BatchId`) VALUES
(1, 2, 'www.immobilienscout24.de/expose/113795369', 'Provisionsfrei BalkonTerrasse', '119000', NULL, ' Aldenrade', NULL, NULL, '31.12.2018', '23:59', ' Duisburg', '1503', NULL, NULL, NULL, '06.10.2019 22:47', NULL),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scraped_data`
--
ALTER TABLE `scraped_data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `scraped_data`
--
ALTER TABLE `scraped_data`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
