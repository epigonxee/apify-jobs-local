-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 03, 2019 at 08:48 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scrap_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `scraped_data`
--

DROP TABLE IF EXISTS `scraped_data`;
CREATE TABLE IF NOT EXISTS `scraped_data` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `AdId` int(100) NOT NULL,
  `AdUrl` varchar(500) NOT NULL,
  `AdInfo` text NOT NULL,
  `AdPrice` varchar(500) NOT NULL,
  `AdPostcode` varchar(500) NOT NULL,
  `AdDistrict` varchar(500) NOT NULL,
  `AdRooms` varchar(500) NOT NULL,
  `AdSquaremeters` varchar(500) NOT NULL,
  `AdDate` varchar(500) NOT NULL,
  `AdTime` varchar(500) NOT NULL,
  `AdCity` varchar(500) NOT NULL,
  `AdPricePerSquaremeters` varchar(500) NOT NULL,
  `AdFromUrl` varchar(500) NOT NULL,
  `ExportDate` varchar(500) NOT NULL,
  `ExportTime` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
