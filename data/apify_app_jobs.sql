-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 22, 2019 at 09:42 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apify_app_jobs`
--

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `testProcedure`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `testProcedure` ()  BEGIN

DECLARE last_run INT;
DECLARE n INT;
DECLARE i INT;
DECLARE count_unique INT;
DECLARE save_ids TEXT;
DECLARE compare_url TEXT;

SET save_ids = "";
SET last_run = 0;

SELECT option_value INTO last_run FROM options WHERE option_name = "last_run_id";

SELECT COUNT(AdUrl) INTO n from scraped_data where id > last_run;

SET i=0;
SET count_unique = 0;

SET last_run= last_run + 1;

SELECT last_run;

IF ( SELECT EXISTS ( SELECT id FROM scraped_data WHERE id = last_run ) ) THEN

    WHILE i<n DO 

        SELECT AdUrl into compare_url FROM scraped_data WHERE id = last_run limit 1;
        	
        IF NOT (SELECT EXISTS( SELECT AdUrl FROM unique_data WHERE AdUrl = compare_url)) THEN
        
        	SET count_unique = count_unique + 1;
            SET save_ids = CONCAT(save_ids , ',' , last_run);

            INSERT INTO unique_data(scraped_id, AdId, AdName , AdUrl, AdInfo, AdPrice, AdPostCode, AdDistrict, AdRooms, AdSquaremeters, AdDate, AdTime, AdCity,
                        AdPricePerSquaremeters, FromUrl, ExportTimeStamp, BatchId)
            SELECT last_run, AdId, AdName , AdUrl, AdInfo, AdPrice, AdPostCode, AdDistrict, AdRooms, AdSquaremeters, AdDate, AdTime, AdCity, AdPricePerSquaremeters,
            		FromUrl, ExportTimeStamp, BatchId
            FROM scraped_data WHERE scraped_data.id = last_run;
            

        END IF;

        SET i = i + 1;
        SET last_run = last_run + 1;

    END WHILE;
    
    SET save_ids = TRIM(LEADING ',' FROM save_ids);
    SELECT last_run;
    SET last_run = last_run - 1;
    SELECT last_run;
    
    UPDATE options SET option_value = last_run WHERE option_name = "last_run_id";
    
END IF;

INSERT INTO logs(statement) VALUES (CONCAT('FOUND ', i , ' NEW Items, COUNT ID ', count_unique ,' Items (', save_ids ,'), copied ', count_unique ,' Items (', save_ids ,')'  ) ); 

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `statement` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(500) NOT NULL,
  `option_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `option_name`, `option_value`) VALUES
(1, 'last_run_id', '0');

-- --------------------------------------------------------

--
-- Table structure for table `scraped_data`
--

DROP TABLE IF EXISTS `scraped_data`;
CREATE TABLE IF NOT EXISTS `scraped_data` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `AdId` int(100) DEFAULT NULL,
  `AdName` varchar(500) DEFAULT NULL,
  `AdUrl` varchar(500) DEFAULT NULL,
  `AdInfo` text,
  `AdPostCode` varchar(500) DEFAULT NULL,
  `AdCity` varchar(500) DEFAULT NULL,
  `AdDate` varchar(500) DEFAULT NULL,
  `AdTime` varchar(500) DEFAULT NULL,
  `FromURL` varchar(500) DEFAULT NULL,
  `AdListCategory` varchar(500) DEFAULT NULL,
  `AdCategory` varchar(500) DEFAULT NULL,
  `AdNumber` varchar(500) DEFAULT NULL,
  `AdOrt` varchar(500) DEFAULT NULL,
  `AdStundenlohn` varchar(500) DEFAULT NULL,
  `AdArt` varchar(500) DEFAULT NULL,
  `AdText` text,
  `AdPersonName` varchar(500) DEFAULT NULL,
  `AdPersonTel` varchar(500) DEFAULT NULL,
  `ExportTimeStamp` varchar(500) DEFAULT NULL,
  `BatchId` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scraped_data`
--

INSERT INTO `scraped_data` (`id`, `AdId`, `AdName`, `AdUrl`, `AdInfo`, `AdPostCode`, `AdCity`, `AdDate`, `AdTime`, `FromURL`, `AdListCategory`, `AdCategory`, `AdNumber`, `AdOrt`, `AdStundenlohn`, `AdArt`, `AdText`, `AdPersonName`, `AdPersonTel`, `ExportTimeStamp`, `BatchId`) VALUES
(1, 1, 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test1', 'test', 'test', '2019-10-15 14:04:37.803218', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `swapper_list`
--

DROP TABLE IF EXISTS `swapper_list`;
CREATE TABLE IF NOT EXISTS `swapper_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `old_value` varchar(500) NOT NULL,
  `new_value` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unique_data`
--

DROP TABLE IF EXISTS `unique_data`;
CREATE TABLE IF NOT EXISTS `unique_data` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `scraped_id` int(255) DEFAULT NULL,
  `AdId` int(100) DEFAULT NULL,
  `AdName` varchar(500) DEFAULT NULL,
  `AdUrl` varchar(500) DEFAULT NULL,
  `AdInfo` text,
  `AdPostCode` varchar(500) DEFAULT NULL,
  `AdCity` varchar(500) DEFAULT NULL,
  `AdDate` varchar(500) DEFAULT NULL,
  `AdTime` varchar(500) DEFAULT NULL,
  `FromURL` varchar(500) DEFAULT NULL,
  `AdListCategory` varchar(500) DEFAULT NULL,
  `AdCategory` varchar(500) DEFAULT NULL,
  `AdNumber` varchar(500) DEFAULT NULL,
  `AdOrt` varchar(500) DEFAULT NULL,
  `AdStundenlohn` varchar(500) DEFAULT NULL,
  `AdArt` varchar(500) DEFAULT NULL,
  `AdText` text,
  `AdPersonName` varchar(500) DEFAULT NULL,
  `AdPersonTel` varchar(500) DEFAULT NULL,
  `ExportTimeStamp` varchar(500) DEFAULT NULL,
  `BatchId` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unique_data`
--

INSERT INTO `unique_data` (`id`, `scraped_id`, `AdId`, `AdName`, `AdUrl`, `AdInfo`, `AdPostCode`, `AdCity`, `AdDate`, `AdTime`, `FromURL`, `AdListCategory`, `AdCategory`, `AdNumber`, `AdOrt`, `AdStundenlohn`, `AdArt`, `AdText`, `AdPersonName`, `AdPersonTel`, `ExportTimeStamp`, `BatchId`) VALUES
(1, NULL, 2, '34', '334', '45', '45', '45', '45', '45', 'ssd', '45', '45', 'asdf', '45', '45', '45', '45', '45', '45', '2019-10-15 14:04:37.803218', '45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', NULL, '$2y$10$0JpSc0gFrHwpzli7W/4sXOmK2f4uZE0yk83QLJYZlZFDwZV4qInVq', 'aLGKMfACJKtLsFV1DFWMx2fz1t2xPw568lqregT7CZmZ7AtgsyGvLRIGY2MW', '2019-10-18 19:00:00', '2019-10-18 19:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
