if (typeof jQuery !== 'undefined')(function( window, document, $, undefined ){

    "use strict";

    var
        self = '',
        selectedId = [],

        APIFY = {

            init: function(){
                self = this;
                self.editFormPopUp();
                self.recordsPerPage();
            },

            editFormPopUp: function(){

                $('.update-filter').on('click', function(){

                    var old_value = $(this).attr("data-old");
                    var new_value = $(this).attr("data-new");
                    var id = $(this).attr("data-id");

                    $('.fil-old').val(old_value);
                    $('.fil-new').val(new_value);
                    $('.fil-id').val(id);

                });
            },

            gridFormSubmit: function(el){
                $(el).closest('form').submit();
            },

            recordsPerPage: function(){
                $('.grids-control-records-per-page').on('change', function(e){
                   self.gridFormSubmit($(this));
                });
            },

            inputMasking : function(){
                $('.usPhoneFax').inputmask({
                    "mask" : "(999) 999-9999"
                })
            },

            dateTimePicker: function () {
                $.datetimepicker.setLocale('en');
                $('.datetimepicker').datetimepicker({
                    format:'Y/m/d H:i',
                    step: 15
                });
            },

            initializeDropZone: function(args){

                //if(!$(args.selector).is('*')) return false;

                try {
                    //  console.log(args.selector);

                    $('input[type="submit"]').hide();

                    Dropzone.autoDiscover = false;
                    var uploadDesign = args.hiddenField;
                    var myDropzone = new Dropzone(args.selector, {
                        url: args.url,
                        init:function(){
                            this.on('success', function(file, responseText){
                                var response = JSON.parse(responseText);
                                var fileuploded = file.previewElement.querySelector("[data-dz-name]");
                                fileuploded.innerHTML = response.message;
                                /*console.log('server');
                                 console.log(fileuploded.innerHTML);*/
                                //file.name =
                            });
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        //paramName: "plan_doc",
                        paramName: args.uploadDocType,
                        //acceptedFiles: ".pdf",
                        acceptedFiles: args.acceptedFiles,
                        previewTemplate: $('#preview-template').html(),
                        thumbnailHeight: 120,
                        thumbnailWidth: 120,
                        maxFiles: (args.selector == '#dropzoneGrabDocs') ? 1 : 500,
                        maxFilesize: 20,
                        addRemoveLinks: true,
                        dictRemoveFile: 'Remove',
                        dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \ <span class="smaller-80 grey">(or click)</span> <br /> \ <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
                        thumbnail: function (file, dataUrl) {
                            // console.log(file);
                            if (file.previewElement) {
                                $(file.previewElement).removeClass("dz-file-preview");
                                var images = $(file.previewElement).find("[data-dz-thumbnail]").each(function () {
                                    var thumbnailElement = this;
                                    thumbnailElement.alt = file.id;
                                    thumbnailElement.src = dataUrl;
                                });
                                setTimeout(function () {
                                    $(file.previewElement).addClass("dz-image-preview");
                                }, 1);
                            }
                        }

                    });

                    myDropzone.on("queuecomplete", function (multiple, i) {
                        $('input[type="submit"]').show();
                        var designUpload = [];
                        jQuery(myDropzone.files).each(function (e, i) {
                            var updatedName = i.previewElement.querySelector("[data-dz-name]").innerHTML;
                            //i.previewElement.id = i.name;
                            i.previewElement.id = updatedName;
                            //if (i.accepted) designUpload.push(i.name);
                            if (i.accepted) designUpload.push(updatedName);
                        });
                        uploadDesign.val(designUpload);

                        if(args.reload) window.location.reload();
                    });

                    myDropzone.on('removedfile', function (file) {
                        var valArr = uploadDesign.val().split(',');
                        var index = valArr.indexOf(file.previewElement.id);
                        if (index > -1) valArr.splice(index, 1);
                        uploadDesign.val(valArr);
                    });

                    //  console.log(myDropzone);

                } catch (e) {
                    console.log(e);
                    //alert('Dropzone.js does not support older browsers!');
                }
            },

            randString: function(length) {
                var result           = '';
                var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                var charactersLength = characters.length;
                for ( var i = 0; i < length; i++ ) {
                    result += characters.charAt(Math.floor(Math.random() * charactersLength));
                }
                return result;
            },

            submitData: function(args){

                var url = $(args).attr('action');
                var method = 'POST';

                $.ajax({
                    url: url,
                    type: method,
                    data:$(args).serialize(),
                    success: function (data) {
                    },
                    error: function () {

                    }
                });
            }
        };

    $(document).ready(function(){
        APIFY.init();
    });

})( window, document, jQuery );