<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScrapedData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scraped_data', function (Blueprint $table) {

            $table->bigIncrements('id')->primary();
            $table->string('AdId');
            $table->text('AdUrl');
            $table->text('AdInfo');
            $table->string('AdPrice');
            $table->string('AdPostcode');
            $table->string('AdDistrict');
            $table->string('AdRooms');
            $table->string('AdSquaremeters');
            $table->string('AdDate');
            $table->string('AdTime');
            $table->string('AdPricePerSquaremeters');
            $table->string('AdFromUrl');
            $table->string('ExportDate');
            $table->string('ExportTime');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scraped_data');
    }
}
