<?php

use App\Helper\CommonHelper;
?>

@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <?php
                    if(session()->has('message')){
                        echo CommonHelper::successAlert(Session::get('message'));
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="content">

        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon"> <a  class="add-resource" href="" title="Add New Filter"> <i class="fa fa-exchange-alt"></i></a></span>
                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>Swap List</h1>
                        </div>
                    </div>
                   <span class="info-box-icon float-right">
                            <a data-toggle="modal" data-target="#modal-create" class="add-resource" href="" title="Add New Filter"> <i class="fa fa-plus"></i></a>
                        </span>
                </div>
            </div>
            <div class="card-body">

                {{--<div class="table-responsive">--}}
                    {{--<table id="user_table" class="table table-bordered table-hover">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}

                            {{--<th style="width: 25%">Old Value</th>--}}
                            {{--<th style="width: 25%">New Value</th>--}}
                            {{--<th style="width: 15%">Action</th>--}}

                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--@foreach($s_lists as $list)--}}

                            {{--<tr>--}}
                                {{--<td>{{$list->old_value}}</td>--}}
                                {{--<td>{{ $list->new_value }}</td>--}}
                                {{--<td>--}}

                                    {{--<a class="update-filter" data-toggle="modal" data-target="#modal-update" data-old="{{$list->old_value}}" data-new="{{ $list->new_value }}" data-id="{{ $list->id }}"  style="color: #17a2b8" href="">--}}
                                        {{--<i class="fas fa-edit"></i>--}}
                                    {{--</a>--}}
                                    {{--<a style="color: #17a2b8" href="{{ route('delete_filter', ['id' => $list->id]) }}"><i class="fas fa-trash"></i></a>--}}

                                {{--</td>--}}

                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}

                    <?php
                        echo $grid;

                    ?>

                </div>

            </div>

        </div>
    </section>

   <?php
        CommonHelper::swapFiltersModal();
        CommonHelper::swapFiltersModal(1);
   ?>

@endsection