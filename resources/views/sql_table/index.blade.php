<?php

use App\Helper\CommonHelper;

?>

@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <?php
                    if(session()->has('message')){
                        echo CommonHelper::successAlert(Session::get('message'));
                    }


                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="content">

        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon"> <a  class="add-resource" href="" title="Add New Data"> <i class="fas fa-database"></i></a></span>
                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>SQL STATEMENTS</h1>
                        </div>
                    </div>
                   {{--<span class="info-box-icon float-right">--}}
                            {{--<a class="add-resource" href="{{ route('a_form_scraped') }}" title="Add New Filter"> <i class="fa fa-plus"></i></a>--}}
                        {{--</span>--}}
                </div>
            </div>

        <div class="card-body">

            @if(isset($error) && !empty($error))
                <div class="error-sql-message">
                    {{ $error  }}
                </div>
            @endif

            <form method="post" action="{{  route('sql_run') }}" enctype="multipart/form-data">
                @csrf

                <div class="row">

                    <div class="col-md-12">

                        <div class="form-group">
                            <div  class="">
                                <textarea style="{{ (isset($error) && !empty($error))?'color:red !important;':'sd'  }}" name="sql_statement" class="form-control" id="" placeholder="SQL QUERY" required>{{ (isset($sql_statement))?$sql_statement:''  }}</textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-info" style="padding-left:6% ;padding-right:6%; float: right">
                            RUN
                        </button>

                    </div>

                </div>

            </form>
            <br>

            @if(isset($results))

                @if(is_array($results) && !empty($results))


                    <div class="table-responsive">

                        <table id="user_table" class="table table-bordered table-hover">

                            <thead>

                                <tr>
                                    @foreach($results[0] as $k => $data)
                                    <th style="width: 25%">{{$k}}</th>
                                    @endforeach
                                </tr>

                            </thead>

                            <tbody>

                                @foreach($results as $k => $data)
                                    <tr>
                                        @foreach($data as $k => $v)
                                            <td>{{$v}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach

                            </tbody>

                        </table>

                    </div>

                @else
                    @if(empty($results) && empty($error))
                        <p>No record found</p>
                    @endif
                @endif

            @endif

         </div>

         </div>






    </section>

@endsection