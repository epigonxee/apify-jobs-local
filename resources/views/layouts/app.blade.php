<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/html">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>
    <!-- Font Awesome -->
    <!-- Font Files are not accessible on Bluehost server, that's why we need to use CDN. -->
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

    {{--<link rel="stylesheet" href="{{asset('plugins/bootstrap/css/font-awesome.min.css')}}">--}}
    <!-- Ionicons -->
    {{--<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">--}}

    <link rel="stylesheet" href="{{asset('plugins/datetimepicker/css/jquery.datetimepicker.min.css')}}">

    <link rel="stylesheet" href="{{asset('plugins/ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/bootstrap3-wysihtml5.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap4.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/icheck.css')}}">
    <link rel="stylesheet" href="{{asset('css/back-end.css')}}">
    <link rel="stylesheet" href="{{asset('js/daterangepicker/daterangepicker-bs3.css')}}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    {{--<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>--}}

</head>



<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand border-bottom navbar-dark bg-info">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
        </ul>



                <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">

            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">

                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-power-off">{{ Auth::user()->name }} </i>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" style="color: #6c757d !important;" href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="get" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>

    </nav>

    <aside class="main-sidebar sidebar-dark-info elevation-4">
        <!-- Brand Logo -->
        <a href="{{ url('/') }}" class="brand-link">
            <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->

                    {{--<li class="nav-item has-treeview hide">--}}
                        {{--<a href="#" class="nav-link ">--}}
                            {{--<i class="nav-icon fa fa-dashboard"></i>--}}

                            {{--<p>--}}
                                {{--Dashboard--}}
                            {{--</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    <li class="nav-item has-treeview">
                        <a href="{{ route('show_swap_lists')  }}"
                           class="nav-link {{ Request::segment(2) === 'swap-lists' ? 'active' : null }}">
                            <i class="nav-icon fa fa-exchange-alt"></i>

                            <p>
                                Swapper List
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="{{ route('scraped_data')  }}"
                           class="nav-link {{ Request::segment(2) === 'scraped-data' ? 'active' : null }}">
                            <i class="nav-icon far fa-clone"></i>

                            <p>
                                Scraped Data
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="{{ route('unique_data')  }}"
                           class="nav-link {{ Request::segment(2) === 'unique-data' ? 'active' : null }}">
                            <i class="nav-icon fas fa-sync-alt"></i>

                            <p>
                                Unique Data
                            </p>
                        </a>
                    </li>

                    <li class="nav-item has-treeview">
                        <a href="{{ route('sql_index')  }}"
                           class="nav-link {{ Request::segment(2) === 'sql-statements' ? 'active' : null }}">
                            <i class="nav-icon fas fa-database"></i>

                            <p>
                                SQL
                            </p>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; {{ date('Y') }} <a href="#">{{ config('app.name') }}</a>.</strong>
        All rights reserved.
    </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery -->

<!-- DataTables -->
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap4.js')}}"></script>
<!-- Bootstrap 4 -->

<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('plugins/datetimepicker/js/jquery.datetimepicker.full.min.js')}}"></script>
<script src="{{asset('plugins/masking/inputmask.min.js')}}"></script>
<script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('js/back-end-script.js')}}"></script>
<script type="text/javascript" src="{{ asset('dist/js/bootstrap-wysihtml5.js') }}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js" integrity="sha256-oE03O+I6Pzff4fiMqwEGHbdfcW7a3GRRxlL+U49L5sA=" crossorigin="anonymous"></script>--}}


<script type="text/javascript" src="{{asset('js/moment/moment-with-locales.js')}}"></script>
<script type="text/javascript" src="{{asset('js/daterangepicker/daterangepicker.js')}}"></script>


<!-- <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>

<script>
    $(function () {
        $("#user_table").DataTable();
        //Initialize Select2 Elements
        $('.select2').select2();
    })
</script> ---->
</body>

</html>