<?php

use App\Helper\CommonHelper;

?>

@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <?php
                    if(session()->has('message')){
                        echo CommonHelper::successAlert(Session::get('message'));
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="content">

        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon"> <a  class="add-resource" href="" title="Add New Data"> <i class="fas fa-sync-alt"></i></a></span>
                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>Unique Data</h1>
                        </div>
                    </div>
                   <span class="info-box-icon float-right">
                            <a class="add-resource" href="{{ route('a_form_unique') }}" title="Add New Filter"> <i class="fa fa-plus"></i></a>
                        </span>
                </div>
            </div>
            <div class="card-body table-responsive">

                <?php
                 echo $grid;
                ?>

                {{--<div class="table-responsive">--}}
                    {{--<table id="user_table" class="table table-bordered table-hover">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}

                            {{--<th style="width: 25%">Ad Id</th>--}}
                            {{--<th style="width: 25%">Ad Name</th>--}}
                            {{--<th style="width: 25%">Ad Url</th>--}}
                            {{--<th style="width: 25%">Ad Price</th>--}}
                            {{--<th style="width: 25%">Ad Post Code</th>--}}
                            {{--<th style="width: 25%">Ad District</th>--}}
                            {{--<th style="width: 25%">Ad Rooms</th>--}}
                            {{--<th style="width: 25%">Ad Squaremeters</th>--}}
                            {{--<th style="width: 25%">Ad Date</th>--}}
                            {{--<th style="width: 25%">Ad Time</th>--}}
                            {{--<th style="width: 25%">Ad City</th>--}}
                            {{--<th style="width: 15%">Action</th>--}}

                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--@foreach($data as $d)--}}

                            {{--<tr>--}}
                                {{--<td>{{$d->AdId}}</td>--}}
                                {{--<td>{{$d->AdName}}</td>--}}
                                {{--<td>{{ $d->AdUrl }}</td>--}}
                                {{--<td>{{ $d->AdPrice }}</td>--}}
                                {{--<td>{{ $d->AdPostCode }}</td>--}}
                                {{--<td>{{ $d->AdDistrict }}</td>--}}
                                {{--<td>{{ $d->AdRooms }}</td>--}}
                                {{--<td>{{ $d->AdSquaremeters }}</td>--}}
                                {{--<td>{{ $d->AdDate }}</td>--}}
                                {{--<td>{{ $d->AdTime }}</td>--}}
                                {{--<td>{{ $d->AdCity }}</td>--}}
                                {{--<td>--}}

                                    {{--<a href="{{ route('u_form_scraped', ['id' => $d->id]) }}" style="color: #17a2b8" >--}}
                                        {{--<i class="fas fa-edit"></i>--}}
                                    {{--</a>--}}
                                    {{--<a style="color: #17a2b8" href="{{ route('delete_scraped_data', ['id' => $d->id]) }}"><i class="fas fa-trash"></i></a>--}}

                                {{--</td>--}}

                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div>--}}


            </div>

        </div>
    </section>

@endsection