<?php

use App\Helper\CommonHelper;

?>

@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <?php
                    if(session()->has('message')){
                        echo CommonHelper::successAlert(Session::get('message'));
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="content">

        <div class="card card-info card-outline">
            <div class="container-fluid mt-2">
                <div class="info-box bg-info">
                    <span class="info-box-icon"> <a class="add-resource" href="{{ route('scraped_data')  }}" title="Add New Data"> <i class="fa fa-arrow-left"></i></a></span>
                    <div class="info-box-content">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <h1>{{ (isset($edit))?'Update':'Add' }} Scraped Data</h1>
                        </div>
                    </div>
                   {{--<span class="info-box-icon float-right">--}}
                            {{--<a class="add-resource" href="" title="Add New Filter"> <i class="fa fa-plus"></i></a>--}}
                        {{--</span>--}}
                {{--</div>--}}
            </div>
            <div class="">

                <form method="post" action="{{ (isset($edit))?route('update_scraped_data', ['id' => $edit->id]):route('add_scraped_data') }}" enctype="multipart/form-data">
                    @csrf
                    <div style="padding: 40px;" class="card-body">

                        <div class="row">

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">AdId</label>
                                    <input type="text" name="AdId" value="{{ (isset($edit))?$edit->AdId:'' }}" class="form-control" id="" placeholder="AdId" required>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdURL</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdUrl" value="{{ (isset($edit))?$edit->AdUrl:'' }}" class="form-control" id="" placeholder="AdURL" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdName</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdName" value="{{ (isset($edit))?$edit->AdName:'' }}" class="form-control" id="" placeholder="AdName" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">AdListCategory</label>
                                    <input type="text" name="AdListCategory" value="{{ (isset($edit))?$edit->AdListCategory:'' }}" class="form-control" id="" placeholder="AdListCategory" required>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdPostCode</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdPostCode" value="{{ (isset($edit))?$edit->AdPostCode:'' }}" class="form-control" id="" placeholder="AdPostCode" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdCategory</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdCategory" value="{{ (isset($edit))?$edit->AdCategory:'' }}" class="form-control" id="" placeholder="AdCategory" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">AdNumber</label>
                                    <input type="text" name="AdNumber" value="{{ (isset($edit))?$edit->AdNumber:'' }}" class="form-control" id="" placeholder="AdNumber" required>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdOrt</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdOrt" value="{{ (isset($edit))?$edit->AdOrt:'' }}" class="form-control" id="" placeholder="AdOrt" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdDate</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdDate" value="{{ (isset($edit))?$edit->AdDate:'' }}" class="form-control" id="" placeholder="AdDate" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">AdTime</label>
                                    <input type="text" name="AdTime" value="{{ (isset($edit))?$edit->AdTime:'' }}" class="form-control" id="" placeholder="AdTime" required>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdCity</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdCity" value="{{ (isset($edit))?$edit->AdCity:'' }}" class="form-control" id="" placeholder="AdCity" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdStundenlohn</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdStundenlohn" value="{{ (isset($edit))?$edit->AdStundenlohn:'' }}" class="form-control" id="" placeholder="AdStundenlohn" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdArt</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdArt" value="{{ (isset($edit))?$edit->AdArt:'' }}" class="form-control" id="" placeholder="AdArt" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdPersonName</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdPersonName" value="{{ (isset($edit))?$edit->AdPersonName:'' }}" class="form-control" id="" placeholder="AdPersonName" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdPersonTel</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="AdPersonTel" value="{{ (isset($edit))?$edit->AdPersonTel:'' }}" class="form-control" id="" placeholder="AdPersonTel" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">FromURL</label>
                                    <input type="text" name="FromURL" value="{{ (isset($edit))?$edit->FromURL:'' }}" class="form-control" id="" placeholder="FromURL" required>
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>ExportTimeStamp</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="ExportTimeStamp" value="{{ (isset($edit))?$edit->ExportTimeStamp:'' }}" class="form-control" id="" placeholder="ExportTimeStamp" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>BatchId</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <input type="text" name="BatchId" value="{{ (isset($edit))?$edit->BatchId:'' }}" class="form-control" id="" placeholder="BatchId" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group">
                                    <label>AdInfo</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <textarea  name="AdInfo" class="form-control" id="" placeholder="AdInfo" required>{{ (isset($edit))?$edit->AdInfo:'' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">

                                <div class="form-group">
                                    <label>AdText</label>
                                    <div class="">
                                        <div class="custom-file">
                                            <textarea  name="AdText" class="form-control" id="" placeholder="AdText" required>{{ (isset($edit))?$edit->AdText:'' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-12 " style="background-color: rgba(0,0,0,.03); padding: 2%">
                        <center>
                            <button type="submit" class="btn btn-info  " style="padding-left:6% ;padding-right:6% ">
                                {{ (isset($edit))?'Update':'Create' }}
                            </button>

                        </center>
                    </div>
                </form>



            </div>

        </div>
        </div>
    </section>

@endsection